/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutantes.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author javier
 */
@Entity
@Table(name = "Individuo")
@NamedQueries({
    @NamedQuery(name = "Individuos.countHuman", query = "select COUNT(a) FROM Individuo a WHERE a.cantSec<2")
    ,
    @NamedQuery(name = "Individuos.countMutant", query = "select COUNT(a) FROM Individuo a WHERE a.cantSec>1")
})
public class Individuo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "adn", nullable = false, columnDefinition = "text CHARACTER SET utf8 COLLATE utf8_general_ci")
    private String adn;

    @Column(columnDefinition = "int default 0", nullable = true)
    private Integer cantSec;

    public Individuo() {
    }

    public Individuo(String adn, Integer mutante) throws JsonProcessingException {
        this.adn = adn;
        this.cantSec = mutante;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdn() {
        return adn;
    }

    public void setAdn(String adn) {
        this.adn = adn;
    }

    public Integer getCantSec() {
        return cantSec;
    }

    public void setCantSec(Integer cantSec) {
        this.cantSec = cantSec;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Individuo)) {
            return false;
        }
        Individuo other = (Individuo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mutantes.entity.Individuo[ id=" + id + " ]";
    }

}
