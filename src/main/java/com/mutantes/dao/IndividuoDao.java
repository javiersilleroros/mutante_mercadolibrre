/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutantes.dao;

import com.mutantes.entity.Individuo;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author javier
 */
@Component
public class IndividuoDao {
    
    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void insert(Individuo a) {
        em.persist(a);
    }

    @Transactional
    public void update(Individuo a) {
        em.merge(a);
    }
    
    public Object getEstadistica() throws Exception{
        Map<String,Number> estado=new HashMap<>();
        try {
            Long human=((Long) em.createNamedQuery("Individuos.countHuman").getSingleResult());
            Long mutant=((Long) em.createNamedQuery("Individuos.countMutant").getSingleResult());
            BigDecimal ratio= new BigDecimal((human>0 && mutant>0)?(Double) mutant.doubleValue()/human.doubleValue():0).setScale(2,RoundingMode.HALF_UP);
            
            estado.put("count_mutant_dna", mutant);
            estado.put("count_human_dna", human);
            estado.put("ratio", ratio);
        } catch (Exception e) {
            estado=null;
            throw new Exception(e.getMessage());
        }
        return estado;
    }
    
}
