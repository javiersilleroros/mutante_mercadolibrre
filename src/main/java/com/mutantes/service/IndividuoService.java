/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutantes.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mutantes.dao.IndividuoDao;
import com.mutantes.entity.Individuo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author javier
 */
@Service
public class IndividuoService {

    @Autowired
    private IndividuoDao individuoDao;

    @Autowired
    ObjectMapper objMapper;
    
    //SECUENCIAS DE 4 LETRAS IGUALES POSIBLES
    private final String[] secuencias= new String[]{"AAAA", "CCCC", "GGGG", "TTTT"};

    /**
     * Metodo principal: ordena la evaluacion en el siguiente orden:
     * 1- Filas
     * 2- Columnas (previa coleccion de las mismas por completo)
     * 3- Diagonales principales (de izquierda a derecha)
     * 4- Diagonales inversas (de derecha a izquierda)
     * Considera el contador de secuencias iguales de cada una de las busquedas
     * para generar cortocircuito en caso de que se encuentren mas de una secuencia
     * de 4 letras iguales.
     * 
     * Considera tambien la variable de evaluacion de las diagonales inversas.
     * @param adn
     * @param diagInv
     * @return contador de la busqueda en todo el adn
     * @throws Exception 
     */
    public Integer isMutant(String[] adn, Boolean diagInv) throws Exception {
        Integer contador = 0;
        int i;
        try {
            Integer tamMatriz = adn.length;
            if (tamMatriz > 3) {
                //Iteracion sobre filas
                i = 0;
                while (contador < 2 && i < adn.length) {
                    contador += evaluaLinea(adn[i], adn[i].length(), contador);
                    i++;
                }
                if (contador < 2) {
                    //Comienza a evaluar el resto de la matriz
                    //Evalua las columnas luego de obtenerlas a todas
                    contador += evaluaColumnas(adn, tamMatriz, contador);
                    if (contador < 2) {
                        contador += evaluaDiagonales(adn, tamMatriz, contador);
                        if (contador < 2 && diagInv == true) {
                            contador += evaluaDiagonalesInversas(adn, tamMatriz, contador);
                        }
                    }
                }
                Individuo individuo = new Individuo(objMapper.writeValueAsString(adn), contador);
                individuoDao.insert(individuo);
            } else {
                throw new Exception("El ADN ingresado no dispone de la dimensión mínima para su evaluación.");
            }

        } catch (Exception e) {
            contador = null;
            throw new Exception(e.getMessage());
        }
        return contador;
    }

    /**
     * Obtiene las columnas de la matriz y luego manda a evaluar cada una de estas
     * @param adn
     * @param tamMatriz
     * @param cuenta
     * @return contador de secuencias iguales resultado de la busqueda actual
     * @throws Exception 
     */
    private Integer evaluaColumnas(String[] adn, Integer tamMatriz, Integer cuenta)
            throws Exception {
        Integer contador = 0;
        String[] columnas = new String[tamMatriz];
        try {
            for (int i = 0; i < tamMatriz; i++) {
                columnas[i] = "";
            }
            for (String string : adn) {
                for (int i = 0; i < tamMatriz; i++) {
                    columnas[i] += string.charAt(i);
                }
            }
            int i = 0;
            while ((cuenta + contador) < 2 && i < columnas.length) {
                contador += evaluaLinea(columnas[i], columnas[i].length(), (cuenta + contador));
                i++;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return contador;
    }

    /**
     * Obtiene las diagonales de izquierda a decha de la matriz
     * y ordena la evaluacion de acada una de estas
     * Cortocitcuito en caso de encontrar mutante
     * @param adn
     * @param tamMatriz
     * @param cuenta
     * @return contador de secuencias iguales resultado de la busqueda actual
     * @throws Exception 
     */
    private Integer evaluaDiagonales(String[] adn, Integer tamMatriz, Integer cuenta) throws Exception {
        Integer cantDiagonales = ((tamMatriz - 4) * 2) + 1;
        String diagonal = "";
        Integer contador = 0;
        try {
            int linea;
            //Diagonal principal
            for (linea = 0; linea < tamMatriz; linea++) {
                diagonal += adn[linea].charAt(linea);
            }
            contador += evaluaLinea(diagonal, diagonal.length(), (cuenta+contador));

            if ((cuenta + contador) < 2) {
                //Diagonales superiores
                //Vuelve a la linea 0 en cada iteracion
                //de abajo hacia arriba (del medio hacia arriba)
                int pos = 0;
                while (pos < (int) cantDiagonales / 2 && (cuenta + contador) < 2) {
                    diagonal = "";
                    int auxPos = pos;
                    linea = 0;
                    while (auxPos + 1 < tamMatriz) {
                        diagonal += adn[linea].charAt(auxPos + 1);
                        auxPos++;
                        linea++;
                    }
                    pos++;
                    contador += evaluaLinea(diagonal, diagonal.length(), (cuenta+contador));
                }
                if ((cuenta + contador) < 2) {
                    //Diagonales inferiores
                    //Vuelve a la posicion 0 en cada iteracion
                    //de eeriba hacia abajo (del medio hacia abajo)
                    linea = 1;
                    while (linea <= (int) cantDiagonales / 2 && (cuenta + contador) < 2) {
                        diagonal = "";
                        int auxLin = linea;
                        pos = 0;
                        while (auxLin < tamMatriz) {
                            diagonal += adn[auxLin].charAt(pos);
                            auxLin++;
                            pos++;
                        }
                        linea++;
                        contador += evaluaLinea(diagonal, diagonal.length(), (cuenta+contador));
                    }
                }
            }

        } catch (Exception e) {
            contador = null;
            throw new Exception(e.getMessage());
        }
        return contador;
    }

    /**
     * Obtiene las diagonales de decha a izquierda de la matriz
     * y ordena la evaluacion de acada una de estas
     * Cortocitcuito en caso de encontrar mutante
     * @param adn
     * @param tamMatriz
     * @param cuenta
     * @return contador de secuencias iguales resultado de la busqueda actual
     * @throws Exception 
     */
    private Integer evaluaDiagonalesInversas(String[] adn, Integer tamMatriz, Integer cuenta) throws Exception {
        Integer cantDiagonales = ((tamMatriz - 4) * 2) + 1;
        String diagonal = "";
        Integer contador = 0;
        try {
            int linea;
            int pos = tamMatriz - 1;
            //Diagonal principal inversa
            for (linea = 0; linea < tamMatriz; linea++) {
                diagonal += adn[linea].charAt(pos);
                pos--;
            }
            //contador += buscaSecuencia(diagonal, getSecuenciaBuscar(diagonal.charAt(0)), diagonal.length(), (cuenta + contador));
            contador += evaluaLinea(diagonal, diagonal.length(), (cuenta+contador));

            if ((cuenta + contador) < 2) {
                //Diagonales inferiores
                //Vuelve a la linea N-1 en todas las iteraciones
                //de arriba hacia abajo (desde el medio hacia abajo)
                pos = 0;
                while (pos < (int) cantDiagonales / 2 && (cuenta + contador) < 2) {
                    diagonal = "";
                    int auxPos = pos;
                    linea = tamMatriz - 1;
                    while (auxPos + 1 < tamMatriz) {
                        diagonal += adn[linea].charAt(auxPos + 1);
                        auxPos++;
                        linea--;
                    }
                    pos++;
                    contador += evaluaLinea(diagonal, diagonal.length(), (cuenta+contador));
                }
                if ((cuenta + contador) < 2) {
                    //Diagonales superiores
                    //Vuelve hasta la posicion 0 en todas las iteraciones
                    //de abajo hacia arriba (desde el medio hacia arriba)
                    linea = tamMatriz - 1;
                    while (linea >= (int) cantDiagonales / 2 && (cuenta + contador) < 2) {
                        diagonal = "";
                        int auxLin = linea;
                        pos = 0;
                        while (auxLin - 1 >= 0) {
                            diagonal += adn[auxLin - 1].charAt(pos);
                            auxLin--;
                            pos++;
                        }
                        linea--;
                        contador += evaluaLinea(diagonal, diagonal.length(), (cuenta+contador));
                    }
                }
            }

        } catch (Exception e) {
            contador = null;
            throw new Exception(e.getMessage());
        }
        return contador;
    }

    /**
     * Ordena la busqueda de cada secuencia posible en una linea
     * @param linea
     * @param tamLinea
     * @param cuenta
     * @return contador de secuencias iguales resultado de la busqueda de la linea
     * @throws Exception 
     */
    public Integer evaluaLinea(String linea, Integer tamLinea, Integer cuenta) throws Exception {
        Integer contador = 0;
        try {
            //String[] secuencias = new String[]{"AAAA", "CCCC", "GGGG", "TTTT"};
            int i = 0;
            while ((contador + cuenta) < 2 && i < 4) {
                contador += buscaSecuencia(linea, secuencias[i], tamLinea, (cuenta+contador));
                i++;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return contador;
    }

    /***
     * Busca una secuencia indicada en la linea
     * acorta la linea si encuenta coincidencia para evaluar el resto de la misma
     * @param linea
     * @param secuencia
     * @param tamLinea
     * @param cuenta
     * @return
     * @throws Exception 
     */
    private Integer buscaSecuencia(String linea, String secuencia, Integer tamLinea, Integer cuenta)
            throws Exception {
        Integer contador = 0;
        try {
            int index = linea.indexOf(secuencia);
            if (index != -1) {
                contador++;
                int resto = tamLinea - (index + 4);
                while (resto > 3 && (cuenta + contador) < 2) {
                    //Continua buscando en la misma linea
                    String subComponente = linea.substring(index + 4);
                    contador += evaluaLinea(subComponente, resto, (cuenta+contador));
                    resto -= 4;
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            contador = null;
            throw new Exception(e.getMessage());
        }
        return contador;
    }

    /***
     * Obtiene la cantidad de humanos y mutantes
     * y el promedio de mutantes sobre humanos evaluados
     * @return
     * @throws Exception 
     */
    public Object stats() throws Exception {
        try {
            return individuoDao.getEstadistica();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}
