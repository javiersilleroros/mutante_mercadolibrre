/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutantes.controller;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mutantes.entity.Individuo;
import com.mutantes.service.IndividuoService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author javier
 */
@RestController
//@RequestMapping(value = "")
public class IndividuoController {
    
    @Autowired
    private IndividuoService individuoService;
    
    @Autowired
    ObjectMapper objMapper;
    
    @RequestMapping(value = "/mutant/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> isMutant(@RequestBody (required = true) HashMap<String,String[]> adn) {
        ResponseEntity<String> resultado;
        try {
            if(individuoService.isMutant(adn.get("dna"),false)>1)
            {
                resultado=new ResponseEntity<>(Boolean.TRUE.toString(),HttpStatus.OK);
            }else{
                resultado=new ResponseEntity<>(Boolean.FALSE.toString(),HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            resultado=new ResponseEntity<>("Error: "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resultado;
    }
    
    @RequestMapping(value = "/mutantD/", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> isMutantD(@RequestBody (required = true) HashMap<String,String[]> adn) {
        ResponseEntity<String> resultado;
        try {
            if(individuoService.isMutant(adn.get("dna"),true)>1)
            {
                resultado=new ResponseEntity<>(Boolean.TRUE.toString(),HttpStatus.OK);
            }else{
                resultado=new ResponseEntity<>(Boolean.FALSE.toString(),HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            resultado=new ResponseEntity<>("Error: "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resultado;
    }
    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> stats() {
        ResponseEntity<Object> resultado;
        try {
            Object resp=individuoService.stats();
            if(resp!=null)
            {
                resultado=new ResponseEntity<>(resp,HttpStatus.OK);
            }else{
                resultado=new ResponseEntity<>("No se pudo obtener estado",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            resultado=new ResponseEntity<>("Error: "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return resultado;
    }
    
}
